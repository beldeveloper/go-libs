package crypto

import "io"

type HashService interface {
	Generate(str string) (string, error)
	GenerateFromStream(r io.Reader) (string, error)
	IsValid(str, hash string) (bool, error)
}
