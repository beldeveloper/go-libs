package aes

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"

	"gitlab.com/beldeveloper/go-libs/errors"
)

func NewCTR(key []byte) CTR {
	return CTR{key: key}
}

type CTR struct {
	key []byte
}

func (c CTR) EncryptStream(dst io.Writer, src io.Reader) error {
	block, err := aes.NewCipher(c.key)
	if err != nil {
		return errors.Wrap(err)
	}

	iv := make([]byte, aes.BlockSize)
	if _, err = rand.Read(iv); err != nil {
		return errors.Wrap(err)
	}
	if _, err = dst.Write(iv); err != nil {
		return errors.Wrap(err)
	}

	stream := cipher.NewCTR(block, iv)
	writer := &cipher.StreamWriter{S: stream, W: dst}

	if _, err = io.Copy(writer, src); err != nil {
		return errors.Wrap(err)
	}

	return nil
}

func (c CTR) DecryptStream(dst io.Writer, src io.Reader) error {
	block, err := aes.NewCipher(c.key)
	if err != nil {
		return errors.Wrap(err)
	}

	iv := make([]byte, aes.BlockSize)
	if _, err = io.ReadFull(src, iv); err != nil {
		return errors.Wrap(err)
	}

	stream := cipher.NewCTR(block, iv)
	reader := &cipher.StreamReader{S: stream, R: src}

	if _, err = io.Copy(dst, reader); err != nil {
		return errors.Wrap(err)
	}

	return nil
}
