package crypto

import "io"

type StreamEncryptor interface {
	EncryptStream(dst io.Writer, src io.Reader) error
}

type StreamDecryptor interface {
	DecryptStream(dst io.Writer, src io.Reader) error
}
