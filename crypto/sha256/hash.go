package sha256

import (
	"crypto/sha256"
	"encoding/hex"
	"io"

	"gitlab.com/beldeveloper/go-libs/errors"
)

func NewHashService(salt string) HashService {
	return HashService{salt: salt}
}

type HashService struct {
	salt string
}

func (s HashService) Generate(str string) (string, error) {
	h := sha256.New()
	_, err := h.Write([]byte(str + s.salt))
	if err != nil {
		return "", errors.Wrap(err)
	}
	return hex.EncodeToString(h.Sum(nil)), nil
}

func (s HashService) GenerateFromStream(r io.Reader) (string, error) {
	h := sha256.New()

	_, err := io.Copy(h, r)
	if err != nil {
		return "", errors.Wrap(err)
	}

	if s.salt != "" {
		_, err = h.Write([]byte(s.salt))
		if err != nil {
			return "", errors.Wrap(err)
		}
	}

	return hex.EncodeToString(h.Sum(nil)), nil
}

func (s HashService) IsValid(str, hash string) (bool, error) {
	validHash, err := s.Generate(str)
	if err != nil {
		return false, err
	}

	return hash == validHash, nil
}
