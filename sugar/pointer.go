package sugar

func PtrToVal[T any](p *T) (res T) {
	if p != nil {
		res = *p
	}
	return
}

func ValToPtr[T any](v T) *T {
	return &v
}
