package sugar

import (
	"os"
	"os/signal"
	"syscall"
)

func WaitShutdown() {
	interruptCh := make(chan os.Signal, 1)
	signal.Notify(interruptCh, os.Interrupt, syscall.SIGTERM)
	<-interruptCh
}
