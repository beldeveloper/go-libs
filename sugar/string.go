package sugar

import (
	"strings"
)

// SkipLines removes n lines from a multiline string after a certain line.
func SkipLines(s string, after, n int) string {
	lines := strings.Split(s, "\n")
	total := len(lines)

	if after > total {
		after = total
	}
	if after+n > total {
		n = total - after
	}

	lines = append(lines[:after], lines[after+n:]...)
	return strings.Join(lines, "\n")
}
