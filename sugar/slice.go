package sugar

func InSlice[T comparable](v T, in []T) bool {
	for _, item := range in {
		if item == v {
			return true
		}
	}
	return false
}
