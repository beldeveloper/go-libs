package sugar

func Ternary[T any](condition bool, ifYes T, ifNo T) T {
	if condition {
		return ifYes
	}
	return ifNo
}
