package http

import (
	"context"
	goerrors "errors"
	"io"
	"net/http"
	"time"

	"github.com/avast/retry-go/v4"
	"gitlab.com/beldeveloper/go-libs/errors"
	"gitlab.com/beldeveloper/go-libs/log"
	"gitlab.com/beldeveloper/go-libs/sugar"
)

const (
	ClientMaxRetryAttempts = 3
	ClientRetryDelay       = time.Millisecond * 100
	DefaultTimeout         = time.Second * 30
)

var errBadGateway = goerrors.New("bad gateway")

type Client interface {
	Get(ctx context.Context, url string) (*http.Response, error)
	PostJson(ctx context.Context, url string, body io.Reader) (*http.Response, error)
	DoRequest(
		ctx context.Context, method, url string, body io.Reader, prepareReq func(req *http.Request),
	) (*http.Response, error)
}

func NewClientWithRetries() ClientWithRetries {
	return ClientWithRetries{
		base: &http.Client{Timeout: DefaultTimeout},
	}
}

type ClientWithRetries struct {
	base   *http.Client
	logger log.Logger
}

func (c ClientWithRetries) Get(ctx context.Context, url string) (*http.Response, error) {
	return c.DoRequest(ctx, http.MethodGet, url, nil, nil)
}

func (c ClientWithRetries) PostJson(ctx context.Context, url string, body io.Reader) (*http.Response, error) {
	return c.DoRequest(ctx, http.MethodPost, url, body, func(req *http.Request) {
		req.Header.Add("Content-Type", "application/json")
	})
}

func (c ClientWithRetries) DoRequest(
	ctx context.Context, method, url string, body io.Reader, prepareReq func(req *http.Request),
) (*http.Response, error) {
	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return nil, errors.Wrap(err)
	}

	if prepareReq != nil {
		prepareReq(req)
	}

	resp, err := c.doAndRetry(req)
	if err != nil {
		return nil, errors.Wrap(err)
	}

	return resp, nil
}

func (c ClientWithRetries) doAndRetry(req *http.Request) (*http.Response, error) {
	var resp *http.Response
	err := retry.Do(
		func() error {
			var err error
			resp, err = c.base.Do(req)
			if err != nil {
				return err
			}

			if sugar.InSlice(resp.StatusCode, []int{
				http.StatusBadGateway,
				http.StatusServiceUnavailable,
				http.StatusGatewayTimeout},
			) {
				return errBadGateway
			}

			return nil
		},

		// options
		retry.RetryIf(func(err error) bool {
			return errors.Is(err, errBadGateway)
		}),
		retry.LastErrorOnly(true),
		retry.Attempts(ClientMaxRetryAttempts),
		retry.Delay(ClientRetryDelay),
		retry.DelayType(func(n uint, err error, config *retry.Config) time.Duration {
			return retry.BackOffDelay(2, err, config)
		}),
		retry.OnRetry(func(n uint, err error) {
			c.logger.With(log.Details{
				"http.method":   req.Method,
				"http.url":      req.URL.String(),
				"retry.attempt": n,
			}).Error(err)
		}),
	)

	return resp, err
}
