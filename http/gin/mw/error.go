package mw

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/beldeveloper/go-libs/errors"
	"gitlab.com/beldeveloper/go-libs/log"
)

const (
	errCodeInvalidBody = "INVALID_BODY"
)

type publicError struct {
	Message string         `json:"message"`
	Code    string         `json:"code,omitempty"`
	Details errors.Details `json:"details,omitempty"`
}

type errorResponse struct {
	Errors []publicError `json:"errors"`
}

func NewError(logger log.Logger) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Next()

		if len(ctx.Errors) == 0 {
			// handle routing errors
			if ctx.Writer.Status() >= 400 {
				ctx.JSON(ctx.Writer.Status(), errorResponse{
					Errors: []publicError{
						{Message: http.StatusText(ctx.Writer.Status())},
					},
				})
			}

			return
		}

		logger = httpLogger(ctx, logger)

		var respStatus int
		var publicErrs []*errors.Error

		for _, e := range ctx.Errors {
			logger.Error(e.Err)
			publicErrs = append(publicErrs, extractErrorList(e.Err)...)
		}

		respErrors := make([]publicError, 0, len(publicErrs))

		for _, publicErr := range publicErrs {
			if respStatus == 0 {
				switch publicErr.Type {
				case errors.BadRequest:
					respStatus = http.StatusBadRequest
				case errors.Unauthorized:
					respStatus = http.StatusUnauthorized
				case errors.Forbidden:
					respStatus = http.StatusForbidden
				case errors.NotFound:
					respStatus = http.StatusNotFound
				case errors.Conflict:
					respStatus = http.StatusConflict
				}
			}

			respErrors = append(respErrors, publicError{
				Message: publicErr.Error(),
				Code:    publicErr.Code,
				Details: publicErr.PublicDetails,
			})
		}

		if respStatus == 0 {
			respStatus = http.StatusInternalServerError
		}

		if len(respErrors) > 0 {
			ctx.JSON(respStatus, errorResponse{Errors: respErrors})
			return
		}

		ctx.Status(respStatus)
	}
}

func extractErrorList(err error) (filtered []*errors.Error) {
	extractor := errors.NewExtractor()

	extractor.SetTypedHandler(func(err *errors.Error) {
		switch err.Type {
		case errors.BadRequest, errors.Unauthorized, errors.Forbidden, errors.NotFound, errors.Conflict:
			filtered = append(filtered, err)
		}
	})

	extractor.SetNativeHandler(func(err error) {
		// parse by error type
		switch tErr := err.(type) {
		case *json.UnmarshalTypeError:
			e := errors.NewErrorf("Invalid request body").
				SetType(errors.BadRequest).
				SetCode(errCodeInvalidBody).
				AddPublicDetails(errors.Details{
					"field":   tErr.Field,
					"expType": tErr.Type.String(),
					"gotType": tErr.Value,
				})
			filtered = append(filtered, e)
			return
		case validator.ValidationErrors:
			for _, vErr := range tErr {
				e := errors.NewErrorf(vErr.Error()).
					SetType(errors.BadRequest).
					SetCode(errCodeInvalidBody)
				filtered = append(filtered, e)
			}
		}

		// parse by text
		switch err.Error() {
		case "unexpected EOF":
			e := errors.NewErrorf("Invalid request body").
				SetType(errors.BadRequest).
				SetCode(errCodeInvalidBody)
			filtered = append(filtered, e)
			return
		}
	})

	extractor.Extract(err)
	return
}
