package mw

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/beldeveloper/go-libs/log"
)

func NewRecovery(logger log.Logger) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		defer func() {
			err := recover()
			if err != nil {
				httpLogger(ctx, logger).Errorf(fmt.Sprint(err))
				ctx.AbortWithStatus(http.StatusInternalServerError)
			}
		}()

		ctx.Next()
	}
}
