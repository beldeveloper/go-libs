package mw

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/beldeveloper/go-libs/log"
)

func httpLogger(ctx *gin.Context, logger log.Logger) log.Logger {
	return logger.With(log.Details{
		"http.request.method":   ctx.Request.Method,
		"http.request.uri":      ctx.Request.RequestURI,
		"http.client.ipAddr":    ctx.ClientIP(),
		"http.client.userAgent": ctx.Request.UserAgent(),
	})
}
