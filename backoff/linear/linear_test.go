package linear

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"
)

const (
	testDur = time.Millisecond * 100
)

func TestIsBlocked(t *testing.T) {
	svc := NewBackoff(Config{
		MaxAttempts:      2,
		WatchDuration:    testDur,
		BlockDurationInc: testDur,
		BlockDurationMin: testDur,
		BlockDurationMax: testDur * 2,
		CleanerFreq:      testDur,
	})
	defer svc.Stop()

	var wg sync.WaitGroup
	n := 5
	wg.Add(n)
	ctx := context.Background()
	for i := 0; i < n; i++ {
		go testIsBlocked(ctx, t, &wg, svc, fmt.Sprintf("key_%d", i))
	}
	wg.Wait()
}

func testIsBlocked(ctx context.Context, t *testing.T, wg *sync.WaitGroup, svc *Backoff, key string) {
	defer wg.Done()
	if svc.Check(ctx, "") != nil {
		t.Fatalf("empty key should be ignored")
	}
	if svc.Check(ctx, key) != nil {
		t.Fatalf("first attempt shouldn't cause block")
	}
	time.Sleep(testDur + time.Millisecond)
	if svc.Check(ctx, key) != nil {
		t.Fatalf("old attempts should be reset")
	}
	if svc.Check(ctx, key) != nil {
		t.Fatalf("second attempt shouldn't cause block")
	}
	if svc.Check(ctx, key) == nil {
		t.Fatalf("third attempt should cause block")
	}
	time.Sleep(testDur + time.Millisecond)
	if svc.Check(ctx, key) != nil {
		t.Fatalf("first attempt shouldn't cause block (previos block is expired)")
	}
	if svc.Check(ctx, key) != nil {
		t.Fatalf("second attempt shouldn't cause block (previos block is expired)")
	}
	if svc.Check(ctx, key) == nil {
		t.Fatalf("third attempt should cause block (previos block is expired)")
	}
	if svc.Check(ctx, key) == nil {
		t.Fatalf("fourth attempt should cause block")
	}
	time.Sleep(testDur + time.Millisecond)
	if svc.Check(ctx, key) == nil {
		t.Fatalf("fifth attempt should cause block because block is still active")
	}
	time.Sleep(testDur*2 + time.Millisecond)
	if svc.Check(ctx, key) != nil {
		t.Fatalf("block should be finally expired")
	}
}
