package linear

import (
	"strings"
	"sync"
	"time"

	"gitlab.com/beldeveloper/go-libs/errors"
	"gitlab.com/beldeveloper/go-libs/sugar"
)

const (
	maxAttempts      = 3                // how many attempts are allowed during watchDuration without blocking
	watchDuration    = time.Second * 20 // how long the attempts are considered active
	blockDurationInc = time.Minute      // while blocked, increase time of the block with every attempt
	blockDurationMin = time.Minute      // first block duration
	blockDurationMax = time.Minute * 10 // max block duration
	cleanerFreq      = time.Minute      // cleaner job frequency

	errCodeTryLater = "TRY_LATER"
)

type backoffRecord struct {
	attempts     int
	firstAttempt time.Time
	blockedTill  *time.Time
	blockDur     time.Duration
}

func (i backoffRecord) isBlocked(now time.Time) bool {
	return i.blockedTill != nil && i.blockedTill.After(now)
}

func (i backoffRecord) isStale(now time.Time, dur time.Duration) bool {
	return !i.isBlocked(now) && i.firstAttempt.Add(dur).Before(now)
}

func DefaultConfig() Config {
	return Config{
		MaxAttempts:      maxAttempts,
		WatchDuration:    watchDuration,
		BlockDurationInc: blockDurationInc,
		BlockDurationMin: blockDurationMin,
		BlockDurationMax: blockDurationMax,
		CleanerFreq:      cleanerFreq,
	}
}

type Config struct {
	MaxAttempts      int
	WatchDuration    time.Duration
	BlockDurationInc time.Duration
	BlockDurationMin time.Duration
	BlockDurationMax time.Duration
	CleanerFreq      time.Duration
}

func NewBackoff(cfg Config) *Backoff {
	svc := &Backoff{
		cfg:    cfg,
		data:   make(map[string]*backoffRecord),
		stopCh: make(chan int),
	}
	go svc.jobClean()
	return svc
}

type Backoff struct {
	cfg Config

	mux  sync.Mutex
	data map[string]*backoffRecord

	stopCh chan int
}

func (s *Backoff) Check(keys ...string) error {
	s.mux.Lock()
	defer s.mux.Unlock()
	var maxBlock *time.Time
	blockMap := make(map[string]bool)
	for _, key := range keys {
		blockMap[key] = false
		if s.isBlocked(key) {
			blockMap[key] = true
			if maxBlock == nil || s.data[key].blockedTill.After(*maxBlock) {
				maxBlock = s.data[key].blockedTill
			}
		}
	}
	if maxBlock != nil {
		return errors.NewErrorf("Too many attempts, try again later").
			SetType(errors.Forbidden).
			SetCode(errCodeTryLater).
			AddPublicDetails(errors.Details{"availableAt": maxBlock.Unix()}).
			AddPrivateDetails(errors.Details{"keys": blockMap})
	}
	return nil
}

func (s *Backoff) isBlocked(key string) bool {
	if key == "" {
		return false
	}
	key = strings.ToLower(key)
	now := time.Now()
	item := s.data[key]
	// if first attempt or previous attempts and block are expired
	if item == nil || item.isStale(now, s.cfg.WatchDuration) {
		// register first attempt
		s.data[key] = &backoffRecord{
			attempts:     1,
			firstAttempt: now,
		}
		return false
	}
	// if not first attempt then increase
	item.attempts++
	// if already blocked
	if item.isBlocked(now) {
		// increase next block duration if not max yet
		if item.blockDur < s.cfg.BlockDurationMax {
			item.blockDur += s.cfg.BlockDurationInc
		}
		// increase block duration
		item.blockedTill = sugar.ValToPtr(now.Add(item.blockDur))
		return true
	}
	// if max attempts are reached
	if item.attempts > s.cfg.MaxAttempts {
		// then block
		item.blockedTill = sugar.ValToPtr(now.Add(s.cfg.BlockDurationMin))
		item.blockDur = s.cfg.BlockDurationMin
		return true
	}
	// reset previous block if expired
	item.blockedTill = nil
	return false
}

func (s *Backoff) jobClean() {
	t := time.NewTicker(s.cfg.CleanerFreq)
	for {
		select {
		case <-t.C:
			s.clean()
		case <-s.stopCh:
			t.Stop()
			return
		}
	}
}

func (s *Backoff) clean() {
	s.mux.Lock()
	defer s.mux.Unlock()
	now := time.Now()
	for k, item := range s.data {
		if item.isStale(now, s.cfg.WatchDuration) {
			delete(s.data, k)
		}
	}
}

func (s *Backoff) Stop() {
	close(s.stopCh)
}
