package backoff

type Checker interface {
	Check(keys ...string) error
}
