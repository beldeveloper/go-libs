package otel

import (
	"gitlab.com/beldeveloper/go-libs/errors"
	"gitlab.com/beldeveloper/go-libs/telemetry"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

func NewSpan(base trace.Span) Span {
	return Span{base: base}
}

type Span struct {
	base trace.Span
}

func (s Span) AddError(err error) telemetry.Span {
	if err == nil {
		return s
	}

	extractor := errors.NewExtractor()

	extractor.SetTypedHandler(func(err *errors.Error) {
		var options []trace.EventOption
		attribs := make([]attribute.KeyValue, 0, 5)

		if err.Type != "" {
			attribs = append(attribs, keyValueToAttrib("error.type", err.Type))
		}

		if err.Code != "" {
			attribs = append(attribs, keyValueToAttrib("error.code", err.Code))
		}

		if err.Stack != "" {
			attribs = append(attribs, keyValueToAttrib("error.stacktrace", err.Stack))
		} else {
			options = append(options, trace.WithStackTrace(true))
		}

		if len(err.PublicDetails) > 0 {
			attribs = append(attribs, keyValueToAttrib("error.publicDetails", err.PublicDetails))
		}

		if len(err.PrivateDetails) > 0 {
			attribs = append(attribs, keyValueToAttrib("error.privateDetails", err.PrivateDetails))
		}

		if len(attribs) > 0 {
			options = append(options, trace.WithAttributes(attribs...))
		}

		s.base.RecordError(err.Original, options...)
	})

	extractor.SetNativeHandler(func(err error) {
		s.base.RecordError(err, trace.WithStackTrace(true))
	})

	s.base.SetAttributes(attribute.Key("error").Bool(true))
	extractor.Extract(err)

	return s
}

func (s Span) AddEvent(name string, attribs telemetry.Attribs) telemetry.Span {
	kvAttribs := make([]attribute.KeyValue, 0, len(attribs))
	for k, v := range attribs {
		kvAttribs = append(kvAttribs, keyValueToAttrib(k, v))
	}
	s.base.AddEvent(name, trace.WithAttributes(kvAttribs...))
	return s
}

func (s Span) AddAttrib(k string, v any) telemetry.Span {
	s.base.SetAttributes(keyValueToAttrib(k, v))
	return s
}

func (s Span) AddAttribs(attribs telemetry.Attribs) telemetry.Span {
	for k, v := range attribs {
		s.base.SetAttributes(keyValueToAttrib(k, v))
	}
	return s
}

func (s Span) End() {
	s.base.End()
}
