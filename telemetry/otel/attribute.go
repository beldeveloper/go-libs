package otel

import (
	"encoding/json"
	"fmt"

	"gitlab.com/beldeveloper/go-libs/telemetry"
	"go.opentelemetry.io/otel/attribute"
	"golang.org/x/exp/constraints"
)

func keyValueToAttrib(k string, v any) (kv attribute.KeyValue) {
	attrib := attribute.Key(k)

	switch tv := v.(type) {
	case bool:
		kv = attrib.Bool(tv)
	case int:
		kv = attrib.Int(tv)
	case []int:
		kv = attrib.IntSlice(tv)
	case uint:
		kv = attrib.Int(int(tv))
	case []uint:
		kv = attrib.IntSlice(convIntSlice(tv))
	case int32:
		kv = attrib.Int(int(tv))
	case []int32:
		kv = attrib.IntSlice(convIntSlice(tv))
	case uint32:
		kv = attrib.Int(int(tv))
	case []uint32:
		kv = attrib.IntSlice(convIntSlice(tv))
	case int64:
		kv = attrib.Int64(tv)
	case []int64:
		kv = attrib.Int64Slice(tv)
	case uint64:
		kv = attrib.Int64(int64(tv))
	case []uint64:
		kv = attrib.IntSlice(convIntSlice(tv))
	case float64:
		kv = attrib.Float64(tv)
	case []float64:
		kv = attrib.Float64Slice(tv)
	case string:
		kv = attrib.String(tv)
	case []string:
		kv = attrib.StringSlice(tv)
	case telemetry.ToJson:
		data, err := json.Marshal(tv.V)
		if err != nil {
			kv = attrib.String(fmt.Sprintf("%+v", tv.V))
		} else {
			kv = attrib.String(string(data))
		}
	default:
		kv = attrib.String(fmt.Sprintf("%+v", v))
	}

	return
}

func convIntSlice[T constraints.Integer](a []T) []int {
	res := make([]int, len(a))
	for i, v := range a {
		res[i] = int(v)
	}
	return res
}
