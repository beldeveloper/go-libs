package jaeger

import (
	"context"

	"gitlab.com/beldeveloper/go-libs/telemetry"
	otelpkg "gitlab.com/beldeveloper/go-libs/telemetry/otel"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.21.0"
	"go.opentelemetry.io/otel/trace"
)

type TracerConfig struct {
	Addr        string
	ServiceName string
	Env         string
	Batch       bool
	Debug       bool
}

func NewTracer(cfg TracerConfig) (Tracer, error) {
	// Create the Jaeger exporter
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(cfg.Addr)))
	if err != nil {
		return Tracer{}, err
	}

	opts := []sdktrace.TracerProviderOption{
		sdktrace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName(cfg.ServiceName),
			attribute.String("environment", cfg.Env),
			attribute.Bool("debug", cfg.Debug),
		)),
	}

	if cfg.Batch {
		opts = append(opts, sdktrace.WithBatcher(exp))
	}

	provider := sdktrace.NewTracerProvider(opts...)
	otel.SetTracerProvider(provider)

	return Tracer{
		provider: provider,
		base:     provider.Tracer(cfg.ServiceName),
	}, nil
}

type Tracer struct {
	provider *sdktrace.TracerProvider
	base     trace.Tracer
}

func (t Tracer) Start(ctx context.Context, name string) (context.Context, telemetry.Span) {
	ctx, span := t.base.Start(ctx, name)
	return ctx, otelpkg.NewSpan(span)
}

func (t Tracer) Stop(ctx context.Context) error {
	return t.provider.Shutdown(ctx)
}
