package noop

import (
	"context"

	"gitlab.com/beldeveloper/go-libs/telemetry"
)

func NewTracer() Tracer {
	return Tracer{}
}

type Tracer struct {
}

func (t Tracer) Start(ctx context.Context, name string) (context.Context, telemetry.Span) {
	return ctx, Span{}
}

func (t Tracer) Stop(ctx context.Context) error {
	return nil
}
