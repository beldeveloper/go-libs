package noop

import (
	"gitlab.com/beldeveloper/go-libs/telemetry"
)

type Span struct {
}

func (s Span) AddError(err error) telemetry.Span {
	return s
}

func (s Span) AddEvent(name string, attribs telemetry.Attribs) telemetry.Span {
	return s
}

func (s Span) AddAttrib(k string, v any) telemetry.Span {
	return s
}

func (s Span) AddAttribs(attribs telemetry.Attribs) telemetry.Span {
	return s
}

func (s Span) End() {
}
