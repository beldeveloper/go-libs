package telemetry

import "context"

type Attribs map[string]any

type ToJson struct {
	V any
}

type Tracer interface {
	Start(ctx context.Context, name string) (context.Context, Span)
	Stop(ctx context.Context) error
}

type Span interface {
	AddError(err error) Span
	AddEvent(name string, attribs Attribs) Span
	AddAttrib(k string, v any) Span
	AddAttribs(attribs Attribs) Span
	End()
}
