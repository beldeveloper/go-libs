package auth

import (
	"time"
)

const (
	ErrCodeJwtTokenIsExpired     = "JWT_TOKEN_IS_EXPIRED"
	ErrCodeJwtTokenIsInvalid     = "JWT_TOKEN_IS_INVALID"
	ErrCodeJwtSignatureIsInvalid = "JWT_SIGNATURE_IS_INVALID"
)

type JwtIssueParams struct {
	ExpiresAt time.Time
	Payload   map[string]any
}

type JwtToken struct {
	Token     string
	Signature string
}

type JwtTokenService interface {
	Issue(p JwtIssueParams) (JwtToken, error)
	Parse(t JwtToken) (map[string]any, error)
}
