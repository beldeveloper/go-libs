package jwt

type Config struct {
	PrivateKey []byte
	PublicKey  []byte
}
