package jwt

import (
	"crypto/rsa"
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"gitlab.com/beldeveloper/go-libs/auth"
	"gitlab.com/beldeveloper/go-libs/crypto"
	"gitlab.com/beldeveloper/go-libs/errors"
)

func NewTokenService(cfg Config, hash crypto.HashService) (TokenService, error) {
	prKey, err := jwt.ParseRSAPrivateKeyFromPEM(cfg.PrivateKey)
	if nil != err {
		return TokenService{}, fmt.Errorf("new token service - parse private key: %w", err)
	}
	pubKey, err := jwt.ParseRSAPublicKeyFromPEM(cfg.PublicKey)
	if nil != err {
		return TokenService{}, fmt.Errorf("new token service - parse public key: %w", err)
	}

	return TokenService{
		prKey:  prKey,
		pubKey: pubKey,
		hash:   hash,
	}, nil
}

type TokenService struct {
	hash   crypto.HashService
	prKey  *rsa.PrivateKey
	pubKey *rsa.PublicKey
}

func (s TokenService) Issue(p auth.JwtIssueParams) (auth.JwtToken, error) {
	signature, signatureHash, err := s.generateSignature()
	if err != nil {
		return auth.JwtToken{}, nil
	}

	pMap := jwt.MapClaims{
		"exp":       float64(p.ExpiresAt.Unix()),
		"signature": signatureHash,
	}
	for k, v := range p.Payload {
		pMap[k] = v
	}

	token, err := jwt.NewWithClaims(jwt.GetSigningMethod("RS256"), pMap).SignedString(s.prKey)
	if err != nil {
		return auth.JwtToken{}, errors.Wrap(err)
	}

	return auth.JwtToken{Token: token, Signature: signature}, nil
}

func (s TokenService) generateSignature() (signature string, hash string, err error) {
	signature, err = s.hash.Generate(uuid.NewString())
	if err != nil {
		return
	}
	hash, err = s.hash.Generate(signature)
	return
}

func (s TokenService) Parse(t auth.JwtToken) (map[string]any, error) {
	jwtToken, err := jwt.Parse(t.Token, func(token *jwt.Token) (interface{}, error) {
		return s.pubKey, nil
	})
	if err != nil {
		jwtErr, ok := err.(*jwt.ValidationError)
		if ok && jwtErr.Errors == jwt.ValidationErrorExpired {
			return nil, errors.NewErrorf("JWT token is expired").
				SetType(errors.Unauthorized).
				SetCode(auth.ErrCodeJwtTokenIsExpired)
		}
		return nil, errors.NewErrorf("JWT token is invalid").
			SetType(errors.Unauthorized).
			SetCode(auth.ErrCodeJwtTokenIsInvalid)
	}

	claims, ok := jwtToken.Claims.(jwt.MapClaims)
	if !ok || !jwtToken.Valid {
		return nil, errors.NewErrorf("JWT token is invalid").
			SetType(errors.Unauthorized).
			SetCode(auth.ErrCodeJwtTokenIsInvalid)
	}

	hash, ok := claims["signature"].(string)
	if !ok {
		return nil, errors.NewErrorf("JWT signature is invalid").
			SetType(errors.Unauthorized).
			SetCode(auth.ErrCodeJwtSignatureIsInvalid)
	}

	hashValid, err := s.hash.IsValid(t.Signature, hash)
	if err != nil {
		return nil, err
	}
	if !hashValid {
		return nil, errors.NewErrorf("JWT signature is invalid").
			SetType(errors.Unauthorized).
			SetCode(auth.ErrCodeJwtSignatureIsInvalid)
	}

	payload := make(map[string]any, len(claims))
	for k, v := range claims {
		if k == "exp" || k == "signature" {
			continue
		}
		payload[k] = v
	}

	return payload, nil
}
