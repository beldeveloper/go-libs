package log

type Details map[string]any

type Logger interface {
	With(d Details) Logger
	Debugf(msg string, v ...any)
	Infof(msg string, v ...any)
	Warnf(msg string, v ...any)
	Errorf(msg string, v ...any)
	Fatalf(msg string, v ...any)
	Error(err error)
	FatalError(err error)
}
