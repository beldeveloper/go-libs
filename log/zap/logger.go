package zap

import (
	"fmt"

	"gitlab.com/beldeveloper/go-libs/errors"
	"gitlab.com/beldeveloper/go-libs/log"
	"go.uber.org/zap"
)

func NewLogger() (Logger, error) {
	config := zap.NewProductionConfig()
	config.DisableStacktrace = true

	base, err := config.Build()
	return Logger{base: base}, err
}

type Logger struct {
	base *zap.Logger
}

func (l Logger) With(d log.Details) log.Logger {
	fields := make([]zap.Field, 0, len(d)+5)

	for k, v := range d {
		fields = append(fields, zap.Any(k, v))
	}

	return Logger{base: l.base.With(fields...)}
}

func (l Logger) Debugf(msg string, v ...any) {
	l.base.Debug(fmt.Sprintf(msg, v...))
}

func (l Logger) Infof(msg string, v ...any) {
	l.base.Info(fmt.Sprintf(msg, v...))
}

func (l Logger) Warnf(msg string, v ...any) {
	l.base.Warn(fmt.Sprintf(msg, v...))
}

func (l Logger) Errorf(msg string, v ...any) {
	l.base.Error(fmt.Sprintf(msg, v...))
}

func (l Logger) Fatalf(msg string, v ...any) {
	l.base.Fatal(fmt.Sprintf(msg, v...))
}

func (l Logger) Error(err error) {
	if err == nil {
		return
	}

	extractor := errors.NewExtractor()

	extractor.SetTypedHandler(func(err *errors.Error) {
		l.base.With(l.errFields(err)...).Error(err.Error())
	})

	extractor.SetNativeHandler(func(err error) {
		l.base.With(zap.String("error.stack", errors.GetStack(5))).Error(err.Error())
	})

	extractor.Extract(err)
}

func (l Logger) FatalError(err error) {
	if err == nil {
		return
	}

	extractor := errors.NewExtractor()

	extractor.SetTypedHandler(func(err *errors.Error) {
		l.base.With(zap.String("error.stack", errors.GetStack(5))).Fatal(err.Error())
	})

	extractor.SetNativeHandler(func(err error) {
		l.Fatalf(err.Error())
	})

	extractor.Extract(err)
}

func (l Logger) Sync() error {
	return l.base.Sync()
}

func (l Logger) errFields(err *errors.Error) []zap.Field {
	fields := make([]zap.Field, 0, 5)

	if err.Type != "" {
		fields = append(fields, zap.String("error.type", string(err.Type)))
	}

	if err.Code != "" {
		fields = append(fields, zap.String("error.code", err.Code))
	}

	if err.Stack != "" {
		fields = append(fields, zap.String("error.stack", err.Stack))
	}

	if len(err.PublicDetails) > 0 {
		fields = append(fields, zap.Any("error.publicDetails", err.PublicDetails))
	}

	if len(err.PrivateDetails) > 0 {
		fields = append(fields, zap.Any("error.privateDetails", err.PrivateDetails))
	}

	return fields
}
