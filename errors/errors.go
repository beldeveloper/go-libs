package errors

import (
	"fmt"
	"strings"
)

func NewErrorList() *ErrorList {
	return &ErrorList{Errors: make([]error, 0, 10)}
}

type ErrorList struct {
	Errors []error
}

func (e *ErrorList) Add(err error) {
	if err == nil {
		return
	}
	if errList, ok := err.(*ErrorList); ok {
		for _, err := range errList.Errors {
			e.Add(err)
		}
		return
	}
	e.Errors = append(e.Errors, err)
}

func (e *ErrorList) Error() string {
	parts := make([]string, len(e.Errors))
	for i, err := range e.Errors {
		parts[i] = fmt.Sprintf("#%d: %s", i+1, err.Error())
	}
	return strings.Join(parts, "\n")
}

func (e *ErrorList) Err() error {
	if len(e.Errors) > 0 {
		return e
	}
	return nil
}
