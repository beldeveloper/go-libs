package errors

import (
	"runtime/debug"

	"gitlab.com/beldeveloper/go-libs/sugar"
)

// GetStack returns a current stack trace.
func GetStack(skipItems int) string {
	stack := string(debug.Stack())
	// don't skip the first line because it contains a goroutine id
	// double lines to skip because each entry in a stack consists of two lines
	return sugar.SkipLines(stack, 1, skipItems*2)
}
