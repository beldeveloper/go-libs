package errors

import (
	"errors"
	"fmt"
)

type (
	Type    string
	Details map[string]any
)

const (
	BadRequest   Type = "badRequest"
	Unauthorized Type = "unauthorized"
	Forbidden    Type = "forbidden"
	NotFound     Type = "notFound"
	Conflict     Type = "conflict"
)

func NewErrorf(msg string, v ...any) *Error {
	return &Error{
		Original: fmt.Errorf(msg, v...),
		Stack:    GetStack(3),
	}
}

type Error struct {
	Original       error
	Type           Type
	Code           string
	PublicDetails  Details
	PrivateDetails Details
	Stack          string
}

func (e *Error) Error() string {
	if e.Original != nil {
		return e.Original.Error()
	}
	return ""
}

func (e *Error) SetType(t Type) *Error {
	e.Type = t
	return e
}

func (e *Error) SetCode(c string) *Error {
	e.Code = c
	return e
}

func (e *Error) AddPublicDetails(f Details) *Error {
	if e.PublicDetails == nil && len(f) > 0 {
		e.PublicDetails = make(Details, len(f))
	}
	for k, v := range f {
		e.PublicDetails[k] = v
	}
	return e
}

func (e *Error) AddPrivateDetails(f Details) *Error {
	if e.PrivateDetails == nil && len(f) > 0 {
		e.PrivateDetails = make(Details, len(f))
	}
	for k, v := range f {
		e.PrivateDetails[k] = v
	}
	return e
}

func Is(err, target error) bool {
	tErr, ok := err.(*Error)
	if ok && errors.Is(tErr.Original, target) {
		return true
	}
	return errors.Is(err, target)
}

func Wrap(err error) *Error {
	tErr, ok := err.(*Error)
	if ok {
		return tErr
	}

	return &Error{
		Original: err,
		Stack:    GetStack(3),
	}
}

func Cast(err error) (tErr *Error, ok bool) {
	tErr, ok = err.(*Error)
	return
}

func AssertType(err error, t Type) bool {
	tErr, ok := Cast(err)
	return ok && tErr.Type == t
}
