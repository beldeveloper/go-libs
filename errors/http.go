package errors

import "net/http"

func ExtractForHttpResponse(err error) (int, []*Error) {
	var pubErrors []*Error

	extractor := NewExtractor()
	extractor.SetTypedHandler(func(err *Error) {
		switch err.Type {
		case BadRequest, Unauthorized, Forbidden, NotFound, Conflict:
			pubErrors = append(pubErrors, err)
		}
	})
	extractor.Extract(err)

	status := http.StatusInternalServerError

	for _, publicErr := range pubErrors {
		switch publicErr.Type {
		case BadRequest:
			status = http.StatusBadRequest
		case Unauthorized:
			status = http.StatusUnauthorized
		case Forbidden:
			status = http.StatusForbidden
		case NotFound:
			status = http.StatusNotFound
		case Conflict:
			status = http.StatusConflict
		}

		if status > 0 {
			break
		}
	}

	return status, pubErrors
}
