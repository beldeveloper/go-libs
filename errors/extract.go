package errors

func NewExtractor() *Extractor {
	return &Extractor{}
}

type Extractor struct {
	nativeHandler func(err error)
	typedHandler  func(err *Error)
}

func (e *Extractor) SetNativeHandler(h func(err error)) {
	e.nativeHandler = h
}

func (e *Extractor) SetTypedHandler(h func(err *Error)) {
	e.typedHandler = h
}

func (e *Extractor) Extract(err error) {
	switch tErr := err.(type) {
	case *ErrorList:
		for _, nestErr := range tErr.Errors {
			e.Extract(nestErr)
		}
	case *Error:
		if errList, ok := tErr.Original.(*ErrorList); ok {
			e.Extract(errList)
			return
		}
		if e.typedHandler != nil {
			e.typedHandler(tErr)
		}
	default:
		if e.nativeHandler != nil {
			e.nativeHandler(err)
		}
	}
}
