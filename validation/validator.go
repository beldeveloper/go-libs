package validation

import (
	"context"
)

const ErrCode = "VALIDATION_ERROR"

type Validator interface {
	Validate(ctx context.Context) error
}
