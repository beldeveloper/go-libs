package validation

import (
	"context"

	"gitlab.com/beldeveloper/go-libs/errors"
)

func NewGroupAll(v ...Validator) *GroupAll {
	return &GroupAll{validators: v}
}

type GroupAll struct {
	validators []Validator
}

func (f *GroupAll) Add(v Validator) {
	f.validators = append(f.validators, v)
}

func (f *GroupAll) Validate(ctx context.Context) error {
	errList := errors.NewErrorList()
	for _, v := range f.validators {
		errList.Add(v.Validate(ctx))
	}
	return errList.Err()
}

func NewGroupAny(v ...Validator) *GroupAny {
	return &GroupAny{validators: v}
}

type GroupAny struct {
	validators []Validator
}

func (f *GroupAny) Add(v Validator) {
	f.validators = append(f.validators, v)
}

func (f *GroupAny) Validate(ctx context.Context) error {
	var err error
	for _, v := range f.validators {
		err = v.Validate(ctx)
		if err == nil {
			return nil
		}
	}
	return err
}
