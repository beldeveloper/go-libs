package validation

import (
	"context"

	"gitlab.com/beldeveloper/go-libs/errors"
)

type number interface {
	int | uint | int8 | uint8 | int16 | uint16 | int32 | uint32 | int64 | uint64 | float32 | float64
}

func NewNumberRequired[T number](field string, value *T) NumberRequired[T] {
	return NumberRequired[T]{field: field, value: value}
}

type NumberRequired[T number] struct {
	field string
	value *T
}

func (v NumberRequired[T]) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == 0 {
		return errors.NewErrorf("'%s' is required", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "required",
				"field":     v.field,
			})
	}
	return nil
}

func NewNumberPositive[T number](field string, value *T) *NumberPositive[T] {
	return &NumberPositive[T]{field: field, value: value}
}

type NumberPositive[T number] struct {
	field string
	value *T

	reversible
}

func (v *NumberPositive[T]) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == 0 {
		return nil
	}

	if !v.reverse && *v.value < 0 {
		return errors.NewErrorf("'%s' must be positive", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "positive",
				"field":     v.field,
			})
	}

	if v.reverse && *v.value > 0 {
		return errors.NewErrorf("'%s' must be negative", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "negative",
				"field":     v.field,
			})
	}

	return nil
}

func NewNumberMin[T number](field string, value *T, min T) NumberMin[T] {
	return NumberMin[T]{field: field, value: value, min: min}
}

type NumberMin[T number] struct {
	field string
	value *T
	min   T
}

func (v NumberMin[T]) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == 0 {
		return nil
	}

	if *v.value < v.min {
		return errors.NewErrorf("'%s' is lower than expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "min",
				"field":     v.field,
				"min":       v.min,
			})
	}

	return nil
}

func NewNumberMax[T number](field string, value *T, max T) NumberMax[T] {
	return NumberMax[T]{field: field, value: value, max: max}
}

type NumberMax[T number] struct {
	field string
	value *T
	max   T
}

func (v NumberMax[T]) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == 0 {
		return nil
	}

	if *v.value > v.max {
		return errors.NewErrorf("'%s' is higher than expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "max",
				"field":     v.field,
				"max":       v.max,
			})
	}

	return nil
}
