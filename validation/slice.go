package validation

import (
	"context"

	"gitlab.com/beldeveloper/go-libs/errors"
	"gitlab.com/beldeveloper/go-libs/sugar"
)

func NewSliceRequired[T any](field string, slice []T) SliceRequired[T] {
	return SliceRequired[T]{field: field, slice: slice}
}

type SliceRequired[T any] struct {
	field string
	slice []T
}

func (v SliceRequired[T]) Validate(ctx context.Context) error {
	if len(v.slice) == 0 {
		return errors.NewErrorf("'%s' is required", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "required",
				"field":     v.field,
			})
	}
	return nil
}

func NewSliceMinLength[T any](field string, slice []T, length int) SliceMinLength[T] {
	return SliceMinLength[T]{field: field, slice: slice, length: length}
}

type SliceMinLength[T any] struct {
	field  string
	slice  []T
	length int
}

func (v SliceMinLength[T]) Validate(ctx context.Context) error {
	if len(v.slice) == 0 {
		return nil
	}

	if len(v.slice) < v.length {
		return errors.NewErrorf("'%s' size is lower than expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "minLength",
				"field":     v.field,
				"length":    len(v.slice),
				"minLength": v.length,
			})
	}

	return nil
}

func NewSliceMaxLength[T any](field string, slice []T, length int) SliceMaxLength[T] {
	return SliceMaxLength[T]{field: field, slice: slice, length: length}
}

type SliceMaxLength[T any] struct {
	field  string
	slice  []T
	length int
}

func (v SliceMaxLength[T]) Validate(ctx context.Context) error {
	if len(v.slice) == 0 {
		return nil
	}

	if len(v.slice) > v.length {
		return errors.NewErrorf("'%s' size is higher than expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "maxLength",
				"field":     v.field,
				"length":    len(v.slice),
				"maxLength": v.length,
			})
	}

	return nil
}

func NewSliceHasAny[T comparable](field string, slice []T, expValues []T) *SliceHasAny[T] {
	return &SliceHasAny[T]{field: field, slice: slice, expValues: expValues}
}

type SliceHasAny[T comparable] struct {
	field     string
	slice     []T
	expValues []T

	reversible
}

func (v *SliceHasAny[T]) Validate(ctx context.Context) error {
	if len(v.slice) == 0 {
		return nil
	}

	var hasAny bool
	for _, val := range v.slice {
		if sugar.InSlice(val, v.expValues) {
			hasAny = true
			break
		}
	}

	if !v.reverse && !hasAny {
		return errors.NewErrorf("'%s' is not expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator":      "hasAny",
				"field":          v.field,
				"expectedValues": v.expValues,
			})
	}

	if v.reverse && hasAny {
		return errors.NewErrorf("'%s' is not expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator":      "hasNotAny",
				"field":          v.field,
				"expectedValues": v.expValues,
			})
	}

	return nil
}

func NewSliceHasAll[T comparable](field string, slice []T, expValues []T) *SliceHasAll[T] {
	return &SliceHasAll[T]{field: field, slice: slice, expValues: expValues}
}

type SliceHasAll[T comparable] struct {
	field     string
	slice     []T
	expValues []T

	reversible
}

func (v *SliceHasAll[T]) Validate(ctx context.Context) error {
	if len(v.slice) == 0 {
		return nil
	}

	var n int
	for _, val := range v.slice {
		if sugar.InSlice(val, v.expValues) {
			n++
		}
	}

	if !v.reverse && len(v.slice) != n {
		return errors.NewErrorf("'%s' is not expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator":      "hasAll",
				"field":          v.field,
				"expectedValues": v.expValues,
			})
	}

	if v.reverse && len(v.slice) == n {
		return errors.NewErrorf("'%s' is not expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator":      "hasNotAll",
				"field":          v.field,
				"expectedValues": v.expValues,
			})
	}

	return nil
}

func NewSliceItemRule[T any](
	field string,
	slice []T,
	rule func(ctx context.Context, i int, item T) error,
) SliceItemRule[T] {
	return SliceItemRule[T]{field: field, slice: slice, rule: rule}
}

type SliceItemRule[T any] struct {
	field string
	slice []T
	rule  func(ctx context.Context, i int, item T) error
}

func (v SliceItemRule[T]) Validate(ctx context.Context) error {
	if len(v.slice) == 0 || v.rule == nil {
		return nil
	}

	errList := errors.NewErrorList()
	for i, item := range v.slice {
		errList.Add(v.rule(ctx, i, item))
	}

	return errList.Err()
}
