package validation

type reversible struct {
	reverse bool
}

func (v *reversible) Not() {
	v.reverse = true
}
