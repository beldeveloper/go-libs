package validation

import (
	"context"
	"net/mail"
	"regexp"
	"strings"
	"time"

	"github.com/nyaruka/phonenumbers"
	"gitlab.com/beldeveloper/go-libs/errors"
)

func NewStringTrim(value *string) StringTrim {
	return StringTrim{value: value}
}

type StringTrim struct {
	value *string
}

func (v StringTrim) Validate(ctx context.Context) error {
	if v.value != nil {
		*v.value = strings.TrimSpace(*v.value)
	}
	return nil
}

func NewStringRequired(field string, value *string) StringRequired {
	return StringRequired{field: field, value: value}
}

type StringRequired struct {
	field string
	value *string
}

func (v StringRequired) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" {
		return errors.NewErrorf("'%s' is required", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "required",
				"field":     v.field,
			})
	}
	return nil
}

func NewStringMinLength(field string, value *string, length int) StringMinLength {
	return StringMinLength{field: field, value: value, length: length}
}

type StringMinLength struct {
	field  string
	value  *string
	length int
}

func (v StringMinLength) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" {
		return nil
	}

	length := len([]rune(*v.value))
	if length < v.length {
		return errors.NewErrorf("'%s' is too short", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "minLength",
				"field":     v.field,
				"length":    length,
				"minLength": v.length,
			})
	}

	return nil
}

func NewStringMaxLength(field string, value *string, length int) StringMaxLength {
	return StringMaxLength{field: field, value: value, length: length}
}

type StringMaxLength struct {
	field  string
	value  *string
	length int
}

func (v StringMaxLength) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" {
		return nil
	}

	length := len([]rune(*v.value))
	if length > v.length {
		return errors.NewErrorf("'%s' is too long", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "maxLength",
				"field":     v.field,
				"length":    length,
				"maxLength": v.length,
			})
	}

	return nil
}

func NewStringContains(field string, value *string, substr string) *StringContains {
	return &StringContains{field: field, value: value, substr: substr}
}

type StringContains struct {
	field  string
	value  *string
	substr string

	reversible
}

func (v *StringContains) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" || len(v.substr) == 0 {
		return nil
	}

	if !v.reverse && !strings.Contains(*v.value, v.substr) {
		return errors.NewErrorf("'%s' doesn't contain substring", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "contains",
				"field":     v.field,
				"substr":    v.substr,
			})
	}

	if v.reverse && strings.Contains(*v.value, v.substr) {
		return errors.NewErrorf("'%s' contains substring", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "notContains",
				"field":     v.field,
				"substr":    v.substr,
			})
	}

	return nil
}

func NewStringStartsWith(field string, value *string, substr string) *StringStartsWith {
	return &StringStartsWith{field: field, value: value, substr: substr}
}

type StringStartsWith struct {
	field  string
	value  *string
	substr string

	reversible
}

func (v *StringStartsWith) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" || len(v.substr) == 0 {
		return nil
	}

	if !v.reverse && !strings.HasPrefix(*v.value, v.substr) {
		return errors.NewErrorf("'%s' doesn't start with substring", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "startsWith",
				"field":     v.field,
				"substr":    v.substr,
			})
	}

	if v.reverse && strings.Contains(*v.value, v.substr) {
		return errors.NewErrorf("'%s' starts with substring", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "notStartsWith",
				"field":     v.field,
				"substr":    v.substr,
			})
	}

	return nil
}

func NewStringEndsWith(field string, value *string, substr string) *StringEndsWith {
	return &StringEndsWith{field: field, value: value, substr: substr}
}

type StringEndsWith struct {
	field  string
	value  *string
	substr string

	reversible
}

func (v *StringEndsWith) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" || len(v.substr) == 0 {
		return nil
	}

	if !v.reverse && !strings.HasSuffix(*v.value, v.substr) {
		return errors.NewErrorf("'%s' doesn't end with substring", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "endsWith",
				"field":     v.field,
				"substr":    v.substr,
			})
	}

	if v.reverse && strings.Contains(*v.value, v.substr) {
		return errors.NewErrorf("'%s' ends with substring", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "notEndsWith",
				"field":     v.field,
				"substr":    v.substr,
			})
	}

	return nil
}

func NewEmail(field string, value *string) Email {
	return Email{field: field, value: value}
}

type Email struct {
	field string
	value *string
}

func (v Email) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" {
		return nil
	}

	if _, err := mail.ParseAddress(*v.value); err != nil {
		return errors.NewErrorf("'%s' is not a valid email", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "email",
				"field":     v.field,
			})
	}

	return nil
}

func NewPhoneNumber(field string, value *string) PhoneNumber {
	return PhoneNumber{field: field, value: value}
}

type PhoneNumber struct {
	field string
	value *string
}

func (v PhoneNumber) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" {
		return nil
	}

	if _, err := phonenumbers.Parse(*v.value, ""); err != nil {
		return errors.NewErrorf("'%s' is not a valid phone number", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "phoneNumber",
				"field":     v.field,
			})
	}

	return nil
}

func NewStringTime(field string, value *string, layout string) StringTime {
	return StringTime{field: field, value: value, layout: layout}
}

type StringTime struct {
	field  string
	value  *string
	layout string
}

func (v StringTime) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" {
		return nil
	}

	if _, err := time.Parse(v.layout, *v.value); err != nil {
		return errors.NewErrorf("'%s' is not a valid time string", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator": "timeString",
				"field":     v.field,
				"expLayout": v.layout,
			})
	}

	return nil
}

func NewStringRegexp(field string, value *string, rx *regexp.Regexp) StringRegexp {
	return StringRegexp{field: field, value: value, rx: rx}
}

type StringRegexp struct {
	field string
	value *string
	rx    *regexp.Regexp
}

func (v StringRegexp) Validate(ctx context.Context) error {
	if v.value == nil || *v.value == "" {
		return nil
	}

	if !v.rx.MatchString(*v.value) {
		return errors.NewErrorf("'%s' doesn't match regexp", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator":  "regexp",
				"field":      v.field,
				"expression": v.rx.String(),
			})
	}

	return nil
}
