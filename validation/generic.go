package validation

import (
	"context"

	"gitlab.com/beldeveloper/go-libs/errors"
)

func NewIn[T comparable](field string, value *T, expValues []T) *In[T] {
	return &In[T]{field: field, value: value, expValues: expValues}
}

type In[T comparable] struct {
	field     string
	value     *T
	expValues []T

	reversible
}

func (v *In[T]) Validate(ctx context.Context) error {
	var defValue T
	if v.value == nil || *v.value == defValue || len(v.expValues) == 0 {
		return nil
	}

	var matches bool
	for _, expValue := range v.expValues {
		if *v.value == expValue {
			matches = true
			break
		}
	}

	if !matches && !v.reverse {
		return errors.NewErrorf("'%s' is not expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator":      "in",
				"field":          v.field,
				"expectedValues": v.expValues,
			})
	}

	if matches && v.reverse {
		return errors.NewErrorf("'%s' is not expected", v.field).
			SetType(errors.BadRequest).
			SetCode(ErrCode).
			AddPublicDetails(errors.Details{
				"validator":      "notIn",
				"field":          v.field,
				"expectedValues": v.expValues,
			})
	}

	return nil
}

func NewCustom(fn func(ctx context.Context) error) Custom {
	return Custom{fn: fn}
}

type Custom struct {
	fn func(ctx context.Context) error
}

func (v Custom) Validate(ctx context.Context) error {
	return v.fn(ctx)
}
