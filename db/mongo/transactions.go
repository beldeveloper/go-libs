package mongo

import (
	"context"

	"gitlab.com/beldeveloper/go-libs/db"
	"gitlab.com/beldeveloper/go-libs/errors"
	"go.mongodb.org/mongo-driver/mongo"
)

func NewTransactions(client *mongo.Client) Transactions {
	return Transactions{client: client}
}

type Transactions struct {
	client *mongo.Client
}

func (r Transactions) New(ctx context.Context) (db.Transaction, error) {
	sess, err := r.client.StartSession()
	if err != nil {
		return nil, errors.Wrap(err)
	}

	tx := mongo.NewSessionContext(ctx, sess)
	err = tx.StartTransaction()
	if err != nil {
		return nil, errors.Wrap(err)
	}

	return &Transaction{SessionContext: tx, ctx: ctx, sess: sess}, nil
}
