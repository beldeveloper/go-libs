package mongo

import (
	"context"
	"sync"

	"gitlab.com/beldeveloper/go-libs/errors"
	"go.mongodb.org/mongo-driver/mongo"
)

type Transaction struct {
	mongo.SessionContext
	ctx  context.Context
	sess mongo.Session

	mux  sync.Mutex
	done bool
}

func (t *Transaction) Rollback() error {
	if !t.check() {
		return nil
	}

	defer t.sess.EndSession(t.ctx)
	err := t.AbortTransaction(t.ctx)
	if err != nil {
		return errors.Wrap(err)
	}

	return nil
}

func (t *Transaction) Commit() error {
	if !t.check() {
		return nil
	}

	defer t.sess.EndSession(t.ctx)
	err := t.CommitTransaction(t.ctx)
	if err != nil {
		return errors.Wrap(err)
	}

	return nil
}

func (t *Transaction) check() bool {
	t.mux.Lock()
	defer t.mux.Unlock()
	done := t.done
	t.done = true
	return !done
}
