package db

import "context"

type Transaction interface {
	context.Context
	Rollback() error
	Commit() error
}

type Transactions interface {
	New(ctx context.Context) (Transaction, error)
}
