package postgres

import (
	"context"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/beldeveloper/go-libs/db"
	"gitlab.com/beldeveloper/go-libs/errors"
)

func NewTransactions(conn *pgxpool.Pool) Transactions {
	return Transactions{conn: conn}
}

type Transactions struct {
	conn *pgxpool.Pool
}

func (t Transactions) New(ctx context.Context) (db.Transaction, error) {
	tx, err := t.conn.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return nil, errors.Wrap(err)
	}
	return &Transaction{Context: ctx, tx: tx}, nil
}
