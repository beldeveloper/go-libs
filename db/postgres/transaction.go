package postgres

import (
	"context"
	"sync"

	"github.com/jackc/pgx/v5"
	"gitlab.com/beldeveloper/go-libs/errors"
)

type Transaction struct {
	context.Context
	tx pgx.Tx

	mux  sync.Mutex
	done bool
}

func (t *Transaction) Rollback() error {
	if !t.check() {
		return nil
	}

	err := t.tx.Rollback(t)
	if err != nil {
		return errors.Wrap(err)
	}

	return nil
}

func (t *Transaction) Commit() error {
	if !t.check() {
		return nil
	}

	err := t.tx.Commit(t)
	if err != nil {
		return errors.Wrap(err)
	}

	return nil
}

func (t *Transaction) check() bool {
	t.mux.Lock()
	defer t.mux.Unlock()
	done := t.done
	t.done = true
	return !done
}
