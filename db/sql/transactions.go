package sql

import (
	"context"
	"database/sql"

	"gitlab.com/beldeveloper/go-libs/db"
	"gitlab.com/beldeveloper/go-libs/errors"
)

func NewTransactions(conn *sql.DB) Transactions {
	return Transactions{conn: conn}
}

type Transactions struct {
	conn *sql.DB
}

func (t Transactions) New(ctx context.Context) (db.Transaction, error) {
	tx, err := t.conn.BeginTx(ctx, nil)
	if err != nil {
		return nil, errors.Wrap(err)
	}
	return &Transaction{Context: ctx, tx: tx}, nil
}
