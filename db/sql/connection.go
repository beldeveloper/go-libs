package sql

import (
	"context"
	"database/sql"
)

type Connection interface {
	ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error)
	QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
}

func Conn(ctx context.Context, conn Connection) (res Connection) {
	res = conn
	if tx, ok := ctx.(Transaction); ok {
		res = tx.tx
	}
	return
}
