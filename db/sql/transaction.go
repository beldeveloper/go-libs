package sql

import (
	"context"
	"database/sql"
	"sync"

	"gitlab.com/beldeveloper/go-libs/errors"
)

type Transaction struct {
	context.Context
	tx *sql.Tx

	mux  sync.Mutex
	done bool
}

func (t *Transaction) Rollback() error {
	if !t.check() {
		return nil
	}

	err := t.tx.Rollback()
	if err != nil {
		return errors.Wrap(err)
	}

	return nil
}

func (t *Transaction) Commit() error {
	if !t.check() {
		return nil
	}

	err := t.tx.Commit()
	if err != nil {
		return errors.Wrap(err)
	}

	return nil
}

func (t *Transaction) check() bool {
	t.mux.Lock()
	defer t.mux.Unlock()
	done := t.done
	t.done = true
	return !done
}
