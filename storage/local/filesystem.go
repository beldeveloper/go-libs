package local

import (
	"io"
	"os"
	"path/filepath"

	"gitlab.com/beldeveloper/go-libs/errors"
)

func NewFilesystem() Filesystem {
	return Filesystem{}
}

type Filesystem struct {
}

func (fs Filesystem) ReadDir(dir string) ([]os.DirEntry, error) {
	entries, err := os.ReadDir(dir)
	if err != nil {
		return nil, errors.Wrap(err).AddPrivateDetails(errors.Details{"dir": dir})
	}

	return entries, nil
}

func (fs Filesystem) WriteStream(path string, r io.Reader) error {
	err := fs.ensureDir(path)
	if err != nil {
		return err
	}

	f, err := os.Create(path)
	if err != nil {
		return errors.Wrap(err).AddPrivateDetails(errors.Details{"path": path})
	}
	defer f.Close()

	_, err = io.Copy(f, r)
	if err != nil {
		return errors.Wrap(err).AddPrivateDetails(errors.Details{"path": path})
	}

	return nil
}

func (fs Filesystem) ReadStream(path string) (io.ReadCloser, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, errors.Wrap(err).AddPrivateDetails(errors.Details{"path": path})
	}

	return f, nil
}

func (fs Filesystem) GetFileInfo(path string) (os.FileInfo, error) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return nil, errors.Wrap(err).AddPrivateDetails(errors.Details{"path": path})
	}

	return fileInfo, nil
}

func (fs Filesystem) Copy(srcPath, dstPath string) error {
	err := fs.ensureDir(dstPath)
	if err != nil {
		return err
	}

	src, err := os.Open(srcPath)
	if err != nil {
		return errors.Wrap(err).AddPrivateDetails(errors.Details{
			"srcPath": srcPath,
			"dstPath": dstPath,
		})
	}
	defer src.Close()

	dst, err := os.Create(dstPath)
	if err != nil {
		return errors.Wrap(err).AddPrivateDetails(errors.Details{
			"srcPath": srcPath,
			"dstPath": dstPath,
		})
	}
	defer dst.Close()

	_, err = io.Copy(dst, src)
	if err != nil {
		return errors.Wrap(err).AddPrivateDetails(errors.Details{
			"srcPath": srcPath,
			"dstPath": dstPath,
		})
	}

	return nil
}

func (fs Filesystem) Remove(path string) error {
	err := os.Remove(path)
	if err != nil {
		return errors.Wrap(err).AddPrivateDetails(errors.Details{"path": path})
	}

	return nil
}

func (fs Filesystem) Rename(oldPath, newPath string) error {
	err := fs.ensureDir(newPath)
	if err != nil {
		return err
	}

	err = os.Rename(oldPath, newPath)
	if err != nil {
		return errors.Wrap(err).AddPrivateDetails(errors.Details{
			"oldPath": oldPath,
			"newPath": newPath,
		})
	}

	return nil
}

func (fs Filesystem) ensureDir(filePath string) error {
	dirPath := filepath.Dir(filePath)
	err := os.MkdirAll(dirPath, os.ModeDir)
	if err != nil && !os.IsExist(err) {
		return errors.Wrap(err).AddPrivateDetails(errors.Details{"filePath": filePath})
	}

	return nil
}
