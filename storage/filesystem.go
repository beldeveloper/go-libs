package storage

import (
	"io"
	"os"
)

type Filesystem interface {
	ReadDir(dir string) ([]os.DirEntry, error)
	WriteStream(path string, r io.Reader) error
	ReadStream(path string) (io.ReadCloser, error)
	GetFileInfo(path string) (os.FileInfo, error)
	Copy(srcPath, dstPath string) error
	Remove(path string) error
	Rename(oldPath, newPath string) error
}
